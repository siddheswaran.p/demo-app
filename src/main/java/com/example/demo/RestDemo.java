package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestDemo {

	@RequestMapping("/greet")
	public String greet() {
		return "Hello ilam!!!";
	}

	@RequestMapping("/")
	public String welcome() {
		return "Hello welcome ilam!!!";
	}
}

